# COREWAR

Ce projet est le projet final de la branche algorithmique de 42.
Il a pour but de nous faire coder un compilateur (ou ASM) de fichiers assembleur en fichiers pouvant etre lus, compris
et executes par la machine virtuelle (ou VM).
Le sujet detaille du projet est disponible [ici](https://cdn.intra.42.fr/pdf/pdf/30/corewar.fr.pdf).

## L'assembleur

Ce programme prend en parametre un fichier source d'un "champion" qui sera execute par la VM.
Ce fichier source est ecrit en assembleur (le langage) et le fichier de destination est un bytecode, d'extension .cor,
lisible par la VM.
L'ASM se lance de la facon suivante:
```
./asm source_file.s
```

## La Machine Virtuelle

La VM lit les fichiers .cor generes par l'ASM, les executes et determine quel champion est vainqueur.
Un champion est concidere comme vainqueur lorsqu'il possede le plus de memoire a la fin du temps imparti ou qu'il est
le dernier a avoir execute une instruction "live" (voir les differentes instructions).
Au debut de la parti, un champion ne possede qu'un seul processus qui execute les instructions en memoire. Ce sont les
processus qui "meurent" si ils ne fond pas de live a temps.
La VM se lance de la facon suivante:
```
./corewar [-dump nbr_cycles] [[-n number] champion1.cor] ...
```

## Les differentes instructions

Les champions, c'est a dire les programmes qui s'affrontent dans la memoire de la VM, peuvent executer differentes
instructions.
Ces instructions sont identifies par leur opcode, leur numero d'operation. Elles peuvent avoir differents types de
parametres et durent un certain nombre de cycle de jeu.

Les instructions sont les suivantes:

| Name  | Opcode | Description |
| ----: | :------- | :------- |
| **live** | 0x01 | Rapporte le joueur designé par le premier parametre comme etant en vie. |
| **ld**   | 0x02 | Charge le premier parametre dans le registre passé en second parametre. |
| **st**   | 0x03 | Charge le contenu du registre passé en premier parametre dans le second parametre. |
| **add**  | 0x04 | Ajoute le second parametre au premier parametre, et stock le resultat dans le troisieme parametre. |
| **sub**  | 0x05 | Soustrait le second parametre au premier parametre, et stock le resultat dans le troisieme parametre. |
| **and**  | 0x06 | Effectue un AND logique entre les deux premiers paramametres et stock le resultat dans le troisieme paramametre. |
| **or**   | 0x07 | Effectue un OR logique entre les deux premiers paramametres et stock le resultat dans le troisieme paramametre. |
| **xor**  | 0x08 | Effectue un XOR logique entre les deux premiers paramametres et stock le resultat dans le troisieme paramametre. |
| **zjmp** | 0x09 | Fait saute le processus a l'adresse passé en parametre. |
| **ldi**  | 0x0a | Charge la valeur a l'adresse resultante de l'addition des deux premiers paramametres, dans le registre passé en troisieme parametre. |
| **sti**  | 0x0b | Charge la valeur contenu dans le registre passé en premier parametre a l'adresse resultante de l'addition des deux derniers paramametres. |
| **fork** | 0x0c | Genere un nouveau processus a l'adresse passée en parametre par copie du processus appelant. |
| **lld**  | 0x0d | Identique a **ld**  mais sans restriction de l'adressage. |
| **lldi** | 0x0e | Identique a **fork**  mais sans restriction de l'adressage. |
| **aff**  | 0x10 | Affiche a l'ecran le char correspondant a la valeure du registre passé en parametre, modulo 256. |

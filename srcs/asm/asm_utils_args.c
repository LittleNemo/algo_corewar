/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_utils_args.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/21 10:51:51 by lbrangie          #+#    #+#             */
/*   Updated: 2019/02/21 10:53:02 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int				asm_get_arg_type(char *arg)
{
	if (arg[0] == CHAR_REGIST)
		return (T_REG);
	else if (arg[0] == CHAR_DIRECT)
		return (T_DIR);
	return (T_IND);
}

int				asm_get_arg_size(t_inst *inst, t_arg *arg)
{
	if (arg->type == T_REG)
		return (1);
	else if (arg->type == T_IND || \
		(arg->type == T_DIR && inst->infos.dir_size == 2))
		return (2);
	return (4);
}

int				asm_check_num(t_arg *fresh)
{
	char			*str;
	int				i;

	str = fresh->str;
	if (fresh->type == T_DIR || fresh->type == T_REG)
		str++;
	if (!*str)
		return (1);
	if (*str == '-')
		str++;
	if (!*str)
		return (1);
	i = -1;
	while (str[++i])
		if (!ft_isdigit(str[i] && str[i] != '-' && str[i] != '+'))
			return (0);
	return (1);
}
